# ECR container scan kickstarter

Python container for kicking off AWS ECR image scans.

Requires the following IAM rights:
```
DescribeRepositories 	
DescribeImages
StartImageScan
DescribeImageScanFindings
```
