import boto3
from datetime import datetime
import os
import logging
import sys
from botocore.errorfactory import ClientError
from botocore.exceptions import WaiterError

logger = logging.getLogger()
handler = logging.StreamHandler()
formatter = logging.Formatter(
    '%(asctime)s %(levelname)-8s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.INFO)


def fetch_repositories(ecr, account_id):
    response = ecr.describe_repositories(registryId=account_id)
    if response.get('ResponseMetadata').get('HTTPStatusCode') != 200:
        logger.fatal("Error fetching repositories")
    return response.get('repositories')


def fetch_images(ecr, repository):
    images = []
    more_images = True
    next_token = ""
    while more_images:
        if next_token:
            response = ecr.describe_images(
                repositoryName=repository,
                maxResults=500,
                nextToken=next_token
            )
        else:
            response = ecr.describe_images(
                repositoryName=repository,
                maxResults=500
            )
        next_token = response.get('nextToken')
        images = images + response.get('imageDetails')
        if not next_token:
            more_images = False
    return images


def start_image_scans(ecr, images):
    for image in images:
        image_digest = image.get('imageDigest')
        repository_name = image.get('repositoryName')
        summary = image.get('imageScanFindingsSummary')
        last_scan_date = ""
        if summary:
            last_scan_date = summary.get(
                'imageScanCompletedAt').date()
        if datetime.today().date() != last_scan_date:
            try:
                ecr.start_image_scan(
                    repositoryName=repository_name,
                    imageId={
                        'imageDigest': image_digest
                    }
                )
            except ClientError:
                logger.warning(
                    f'Image {image_digest} in repository {repository_name} has exceeded the scan quotum for current time window.')
        else:
            logger.info(
                f'Image {image_digest} in repository {repository_name} has already been scanned today.')


def await_scan_results(ecr, images):
    waiter = ecr.get_waiter('image_scan_complete')
    for image in images:
        image_digest = image.get('imageDigest')
        repository_name = image.get('repositoryName')
        try:
            waiter.wait(
                repositoryName=repository_name,
                imageId={
                    'imageDigest': image_digest
                }
            )
        except ClientError:
            logger.warning('Reached timeout waiting for scan results')
        except WaiterError:
            logger.warning('Error scanning image')


def retrieve_findings(ecr, images):
    findings = {}
    for image in images:
        image_digest = image.get('imageDigest')
        repository_name = image.get('repositoryName')
        more_findings = True
        next_token = ""
        while more_findings:
            if next_token:
                response = ecr.describe_image_scan_findings(
                    repositoryName=repository_name,
                    imageId={
                        'imageDigest': image_digest
                    },
                    maxResults=500,
                    nextToken=next_token
                )
            else:
                response = ecr.describe_image_scan_findings(
                    repositoryName=repository_name,
                    imageId={
                        'imageDigest': image_digest
                    },
                    maxResults=500
                )
            next_token = response.get('nextToken')
            findings.update(response)
            if not next_token:
                more_findings = False
    return findings


def publish_finding_data(sns, topic, message):
    sns.publish(
        TopicArn=topic,
        Message=message,
        Subject="Notification from ECR Container Scan"
    )


if __name__ == "__main__":
    sns_alert_topic = os.environ.get('SNS_ALERT_TOPIC_ARN')
    region = os.environ.get('AWS_DEFAULT_REGION', default='eu-west-1')

    ecr = boto3.client('ecr', region)
    sts = boto3.client('sts', region)
    sns = boto3.client('sns', region)

    account_id = os.environ.get(
        'ACCOUNT_ID', default=sts.get_caller_identity().get('Account'))

    logger.info('Retrieving repositories')
    repositories = fetch_repositories(ecr, account_id)

    if not repositories:
        logger.fatal('No repositories found')
        sys.exit(1)

    results = []
    amount_of_images = 0
    for repository in repositories:
        repository_name = repository.get('repositoryName')
        registry = repository.get('registryId')

        logger.info(
            f'Fetching images for registry {registry}, repository {repository_name}')
        images = fetch_images(ecr, repository_name)
        amount_of_images += len(images)

        # Only try scanning if there's anything to be scanned
        if len(images) == 0:
            logger.warning(f'No images found')
            continue

        logger.info(
            f'Starting image scans for registry {registry}, repository {repository_name}')
        start_image_scans(ecr, images)

        logger.info(
            f'Awaiting image scans for registry {registry}, repository {repository_name}')
        await_scan_results(ecr, images)

        logger.info(
            f'Retrieving findings for registry {registry}, repository {repository_name}')
        results.append(retrieve_findings(ecr, images))

    high = 0
    critical = 0
    failed_repositories = []
    for result in results:
        scan_status = result.get('imageScanStatus').get('status')
        if scan_status == 'FAILED':
            failed_repositories.append(result.get('repositoryName') + ",")
            continue
        has_completed_scan = result.get('imageScanFindings')
        if not has_completed_scan:
            continue
        has_results = result.get('imageScanFindings').get(
            'findingSeverityCounts')
        if has_results:
            has_cricical = result.get('imageScanFindings').get(
                'findingSeverityCounts').get('CRITICAL')
            has_high = result.get('imageScanFindings').get(
                'findingSeverityCounts').get('HIGH')
            if has_cricical:
                critical += has_cricical
            if has_high:
                high += has_high

    if failed_repositories == []:
        logger.info('Publishing amount of critical and high findings')
        message = f"ECR scan for account ID {account_id} on {amount_of_images} images resulted in {critical} critical and {high} high findings"
    else:
        logger.info('Publishing failed repositories')
        message = (
            f'The following repositories have failed scanning:{failed_repositories}. ECR scan on {amount_of_images} images resulted in {critical} critical and {high} high findings')

    logger.info(f'Results: {message}')

    if sns_alert_topic:
        logger.info('Publishing amount of critical and high findings to SNS')
        publish_finding_data(sns, sns_alert_topic, message)
